# AI Snake
This project is an example of a game of multi-player snake. This was to evaluate the success of 2 intelligent algorithms - MiniMax and A* - against 2 snakes moving randomly.

To run, compile then call "$ javac snake/snake"

