package players;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import snake.GameState;
import snake.Snake;

/*
Steps for minimax evaluation
1. copy current state
2. find legal moves
3. make first move
4. calculate minimax score 
5. increment snake index
6. copy game state and make second move
7. continue steps 4-6 until depth reached

Number of wins output into text file
My run gave 7/20
*/

public class MiniMaxPlayer extends RandomPlayer {

	public MiniMaxPlayer(GameState state, int index, Snake game) {
		super(state, index, game);
	}

	public void doMove() {
		int move = 0; int lastMove = 1;	// first move is north as program is loading all the classes so may take too long to chooseMove at first
		long availableTime = 200;
		int depth = 1;
		long startTime = System.currentTimeMillis();
		long usedTime = 0; long lastUsedTime = 0;

		/* Iterative Deepening */
		while (true) {
			depth++;
			move = chooseMove(depth);
			usedTime = System.currentTimeMillis() - startTime;
			if (usedTime > availableTime) {
				break;
			}
			lastMove = move;
			lastUsedTime = usedTime;
		}
		/* */
		state.setOrientation(index, lastMove);
		System.out.println("Move " + lastMove + " found at depth " + depth + " made in " + lastUsedTime + "ms");	// Shows move, depth and time taken. Can be commented out
	}

	private int chooseMove(int maxDepth) {
		double bestScore = -100;
		int bestMove = 0;
		int parentIndex = this.index;
		// Minimax for possible up, down, left, right
		List<Integer> legalMoves = getLegalMoves(parentIndex, state);
		double snakeUtilities[] = new double[state.getNrPlayers()];
		Arrays.fill(snakeUtilities, -100.0);
		for (int move: legalMoves) {
			int depth = maxDepth;

			GameState next = new GameState(state);
			next.setOrientation(parentIndex, move);
            next.updatePlayerPosition(parentIndex);
			
			double score = getMaxScore(next, move, depth, snakeUtilities, parentIndex);
			
			if (state.getTargetX() == next.getPlayerX(parentIndex).get(0) &&
			state.getTargetY() == next.getPlayerY(parentIndex).get(0)) {
				state.setOrientation(parentIndex, move);
				return move;
			}

			if (score > bestScore || bestMove == 0) {
				bestMove = move;
				bestScore = score;
			}
		
		}
		state.setOrientation(parentIndex, bestMove);
        return bestMove;
	}


	private double getMaxScore(GameState s, int prevMove, int depth, double[] parentUtilities, int parentIndex) {
		int childIndex = (parentIndex + 1) % s.getNrPlayers();
		if (depth == 0 || s.isGameOver()) {
			return evaluateState(s, parentUtilities, parentIndex);
		}
		List<Integer> legalMoves = getLegalMoves(childIndex, s);
		for (int move: legalMoves) {
			double childUtilities[] = new double[s.getNrPlayers()];
			Arrays.fill(childUtilities, -100.0);

			double score = 0;
			/* Disc is eaten, create chance nodes */
			if (!s.hasTarget()) {
				// Create array to store cumulative utilities
				double[] chanceUtilities = new double[s.getNrPlayers()];
				for (int i = 0; i < 5; i++) {
					GameState next = new GameState(s);
					next.chooseNextTarget();
					next.setOrientation(childIndex, move);
					next.updatePlayerPosition(childIndex);
					score += getMaxScore(next, move, depth-1, chanceUtilities, childIndex);
					for (int ix = 0 ; ix < s.getNrPlayers(); ix++) { chanceUtilities[ix] += parentUtilities[ix]; }
				}
				// divide every value in cumulative utilities by 5
				for (int ix = 0 ; ix < s.getNrPlayers(); ix++) { 
					chanceUtilities[ix] /= 5.0; 
					parentUtilities[ix] = chanceUtilities[ix];
				}
			}
			/* normal Minimax with multiple players */
			else {
				GameState next = new GameState(s);
				next.setOrientation(childIndex, move);
				next.updatePlayerPosition(childIndex);
				score = getMaxScore(next, move, depth - 1, childUtilities, childIndex);
			}
			/* Propagate Utilities if move score is better for parentNode */
			if (childUtilities[parentIndex] > parentUtilities[parentIndex]) {
				for (int j = 0; j < parentUtilities.length; j++) {
					parentUtilities[j] = childUtilities[j];
				} 
			}
		}
		return evaluateState(s, parentUtilities, parentIndex);
	}


	public double evaluateState(GameState s, double[] utilities, int index) {
		for (int snakeId = 0; snakeId < s.getNrPlayers(); snakeId++) {
			double score = 0.0;
			if (s.isDead(snakeId)) {
				score -= 500;
				continue;
			}

			// find manhattan distance from head to target (large is bad)
			double distanceAway = Math.abs(s.getPlayerX(snakeId).get(0) - s.getTargetX()) + Math.abs(s.getPlayerY(snakeId).get(0) - s.getTargetY());
			score -= distanceAway;
			
			// find distance from other heads, further is good
			for (int i = 0; i < s.getNrPlayers(); i++) {
				if (i == snakeId || s.isDead(i)) { continue; }
				double distanceFromHead = Math.abs(s.getPlayerX(i).get(0) - s.getPlayerX(snakeId).get(0)) + Math.abs(s.getPlayerY(i).get(0) - s.getPlayerY(snakeId).get(0));
				score += distanceFromHead/5;
			}

			/** 
			 *  Looked at implementing a way to prevent snake from trapping itself, e.g. keeping it away from it's tail
			 *  However couldn't find a way to do so without making the algorithm perform worse
			 * */

			// snake eats disc - v good
			score += s.getSize(snakeId) * 10;

			utilities[snakeId] = score;
		}
		return utilities[index];	// probably not needed
	}


	public List<Integer> getLegalMoves(int snakeIndex, GameState state) {
		List<Integer> legalMoves = new ArrayList<>();
		if (!state.isDead(snakeIndex)) {
			for (int i = 1; i <= 4; i++) {
				if (state.isLegalMove(snakeIndex, i)) {
					legalMoves.add(i);
				}
			}
		}
        return legalMoves;
	}

}